package SpeedTest;

import com.speed.BasePage;
import com.speed.StaticProvider;
import org.testng.annotations.Test;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static com.speed.Output.writeToFile;
import static com.speed.SujetCheck.SujetCheck;
import static com.speed.SujetCheck.sujetCheckRun;
import static java.lang.System.currentTimeMillis;

public class SpeedTest extends BasePage {

    @Test
    public void SujetCheckTest() throws Exception {

        List<Long> timeList = new ArrayList<>();

        int time = 1;
        do {
            openChrome();
            open(SujetCheck);
            long start = currentTimeMillis();
            sujetCheckRun();
            long finish = currentTimeMillis();
            long totalTime = finish - start;
            showAttemps(time,totalTime);
            timeList.add(totalTime);
            time++;
            closeBrowser();
        } while (time < 6);




        int sum = 0;

        for (Long aTimeList : timeList) sum = (int) (sum + aTimeList);

        int average = sum / timeList.size();

        printResult(average, "SujetCheck", "run");
        writeToFile(average, "SujetCheck", "run");

    }

    @Test(dataProvider = "pages", dataProviderClass = StaticProvider.class, groups = {"cached"})
    public void CachedTest(String nameOfUrl, Object url) throws IOException {

        List<Long> timeList = new ArrayList<>();

        openChrome();
        open(url);

        int time = 1;
        do {
            long start = currentTimeMillis();
            driver.navigate().refresh();
//            driver.findElement(cssSelector(".logo-main.site_1")).isDisplayed();
            long finish = currentTimeMillis();
            long totalTime = finish - start;
            showAttemps(time,totalTime);
            timeList.add(totalTime);
            time++;
        } while (time < 6);

        closeBrowser();

        int sum = 0;

        for (Long aTimeList : timeList) sum = (int) (sum + aTimeList);

        int average = sum / timeList.size();

        printResult(average, nameOfUrl, "cached");
        writeToFile(average, nameOfUrl, "cached");

    }

    @Test(dataProvider = "pages", dataProviderClass = StaticProvider.class, groups = {"notCached"})
    public void NotCachedTest(String nameOfUrl, Object url) throws IOException {

        List<Long> timeList = new ArrayList<>();

        int time = 1;
        do {
            openChrome();
            long start = currentTimeMillis();
            open(url);
//            driver.findElement(cssSelector(".logo-main.site_1")).isDisplayed();
            long finish = currentTimeMillis();
            long totalTime = finish - start;
            showAttemps(time,totalTime);
            timeList.add(totalTime);
            time++;
            closeBrowser();
        } while (time < 6);

        int sum = 0;

        for (Long aTimeList : timeList) sum = (int) (sum + aTimeList);

        int average = sum / timeList.size();

        printResult(average, nameOfUrl, "not cached");
        writeToFile(average, nameOfUrl, "not cached");

    }
}