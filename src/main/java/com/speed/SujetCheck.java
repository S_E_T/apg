package com.speed;

import org.openqa.selenium.By;

import java.net.URL;

import static com.speed.BasePage.driver;
import static java.lang.System.currentTimeMillis;

public class SujetCheck {

    public final static String SujetCheck    = "http://www.apgsga.ch/de/angebot/tools/sujetcheck/";

    public static String file = "/Users/serhiisydorenko/IdeaProjects/APG/src/main/resources/dummy.jpg";

    public static URL classLoader = ClassLoader.getSystemClassLoader().getResource("dummy.jpg");

    public static void sujetCheckRun(){

        driver.findElement(By.cssSelector(".input-radiocheck.uniform")).click();
        driver.findElement(By.xpath("html/body/div[2]/div/div/div[3]/form/div[2]/input")).click();
        driver.findElement(By.cssSelector(".font-light")).isDisplayed();
        driver.findElement(By.cssSelector(".input-file")).sendKeys(file);
        System.out.println("PATH IS:" +  classLoader.getPath());
        long startInMthod = currentTimeMillis();
        driver.findElement(By.xpath("//*[@id=\"top\"]/div[2]/div/div/div[3]/form/div[3]/input")).isDisplayed();
        driver.findElement(By.xpath("//*[@id=\"top\"]/div[2]/div/div/div[3]/form/div[3]/input")).click();
        driver.findElement(By.xpath("html/body/div[2]/div/div/div[3]/div[1]/div/div/ul/li[1]/img")).isDisplayed();
        long finishInMethod = currentTimeMillis();
        long totalTimeInThod = finishInMethod - startInMthod;
        System.out.println(totalTimeInThod + "TIME IN METHOD");
        System.out.println();
    }

}
