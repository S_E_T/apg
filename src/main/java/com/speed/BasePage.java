package com.speed;

import io.github.bonigarcia.wdm.ChromeDriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;

import java.io.IOException;

import static com.speed.Output.eraseFile;
import static java.lang.System.out;

public class BasePage {

    protected static WebDriver driver;

    @BeforeTest (alwaysRun = true)
    public static void setupClass() throws IOException {
        ChromeDriverManager.getInstance().setup();
        eraseFile();
    }

    @AfterTest (alwaysRun = true)
    public static void tearDown(){
        driver.quit();
    }

    protected static void openChrome() {
        driver = new ChromeDriver();
    }

    protected static void closeBrowser(){
        driver.close();
        driver.quit();
    }

    protected static void open(Object url){
        driver.get(String.valueOf(url));
    }

    protected static void showAttemps(int time, long totalTime){
        out.println("Attempt " + time + " is finished");
        out.println("It's time is: " + totalTime);
    }

    protected static void printResult(Integer average, Object nameOfUrl, String type){
        out.println("This was " + type + " " + nameOfUrl);
        out.println("Average time load: " + average + " milliseconds");
    }
}