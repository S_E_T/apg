package com.speed;

import java.io.*;

public class Output {

    private static final String nameOfFile = "text.txt";

    static void eraseFile() throws IOException {

        File file = new File(nameOfFile);

        Writer output;
        output = new BufferedWriter(new FileWriter(file));  //clears file every time
        output.close();

    }

    public static void writeToFile(Integer average, String URL, String type) throws IOException {

        File file = new File(nameOfFile);

        Writer output;
        output = new BufferedWriter(new FileWriter(file, true));

        output.write("//////////////////////" + "\n");
        output.write("This was " + type + " " + URL + "\n");
        output.write("Average time load: " + average + " milliseconds" + "\n" + "\n");

        output.close();

    }
}